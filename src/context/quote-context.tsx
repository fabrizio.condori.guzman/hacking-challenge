import { Dispatch, SetStateAction, useState } from "react";
import { createContext } from '../library/utils/createContext';
import { Person } from "../models/person.model";

export interface PersonContextType {
    person: Person | null;
    setPerson: Dispatch<SetStateAction<Person | null>>;
}

export type StepType = 'HOME' | 'PLAN' | 'CONFIRMATION'
export interface ContextType{
    person: Person | null;
    setPerson: Dispatch<SetStateAction<Person | null>>;
    step: StepType
    setStep: Dispatch<SetStateAction<StepType>>;
    amount: number,
    setAmount: Dispatch<SetStateAction<number>>;
}

export const [useContext, Provider] = createContext<ContextType>()

export const QuoteContextProvider = ({children}: {children: React.ReactNode}) => {
    const [person, setPerson] = useState<Person | null>(null)
    const [step, setStep] = useState<StepType>('HOME')
    const [amount, setAmount] = useState<number>(0)
    
    const valueProvider: ContextType = {
        person,
        setPerson,
        step,
        setStep,
        amount,
        setAmount
    }

    return (
        <Provider value={valueProvider}>
            {children}
        </Provider>
    )
}