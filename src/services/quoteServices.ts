import axios from 'axios';
import { Car } from '../models/car.model';
import { CoverageTypeModel } from '../models/coverage.model';
import { Person } from '../models/person.model';

export const getUser = async():Promise<Person> => {
    const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/1')

    return data
}

export const getCarDetailsByPlate = async(plate: string): Promise<Car> => {
    const data: Car = {
        brand: 'Woolkswagen',
        year: 2019,
        model: 'Golf'
    }

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(data)
        }, 700)
    })
}

export const getCoverageData = (): CoverageTypeModel[] => {
    return [
        {
            id: 1,
            name: 'Protege a tu auto',
            items: [
                {
                    id: 1,
                    name: 'Llanta robada',
                    description: 'He salido de casa a las cuatro menos cinco para ir a la academia de ingles de mi pueblo (Sant Cugat, al lado de Barcelona) con mi bici, na llego a la academia que está en el centro del pueblo en una plaza medio-grande y dejo donde siempre la bici atada con una pitón a un sitio de esos de poner las bicis y mucho más',
                    amount: 15
                },
                {
                    id: 2,
                    name: 'Choque y/o pasarte la luz roja ',
                    description: 'He salido de casa a las cuatro menos cinco para ir a la academia de ingles de mi pueblo (Sant Cugat, al lado de Barcelona) con mi bici, na llego a la academia que está en el centro del pueblo en una plaza medio-grande y dejo donde siempre la bici atada con una pitón a un sitio de esos de poner las bicis y mucho más',
                    amount: 20
                },
                {
                    id: 3,
                    name: 'Atropello en la vía Evitamiento ',
                    description: 'He salido de casa a las cuatro menos cinco para ir a la academia de ingles de mi pueblo (Sant Cugat, al lado de Barcelona) con mi bici, na llego a la academia que está en el centro del pueblo en una plaza medio-grande y dejo donde siempre la bici atada con una pitón a un sitio de esos de poner las bicis y mucho más',
                    amount: 50
                },
            ]
        },
        {
            id: 2,
            name: 'Protege a los que te rodean',
            items: []
        },
        {
            id: 3,
            name: 'Mejora tu plan',
            items: []
        }
    ]
}