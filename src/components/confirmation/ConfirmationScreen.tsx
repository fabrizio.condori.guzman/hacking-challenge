import React, { CSSProperties, useEffect } from 'react';
import { useContext } from '../../context/quote-context';
import { Header } from '../../library/common/components/Header';
import { ConfirmationContent } from './ConfirmationContent';
import { ConfirmationImage } from './ConfirmationImage';

const headerStyles: CSSProperties = {
    backgroundColor: 'white',
    borderBottom: '1px solid #E4E8F7'
}

export const ConfirmationScreen = () => {
    const { amount } = useContext()

    useEffect(() => {
        console.log({amount})
    }, [amount])
    
    return (
        <div className="confirmation">
            <Header {...headerStyles}/>
            <ConfirmationImage/>
            <ConfirmationContent/>
        </div>
    )
}
