import React from 'react';
import { useContext } from '../../context/quote-context';
import { AppSettings } from '../../library/settings';

export const ConfirmationContent = () => {
    const { person } = useContext()

    return (
        <div className="confirmation__content">
            <div className="confirmation__content--block">
                <h2 className="confirmation__content__title"><span>¡Te damos la bienvenida!</span><br /> Cuenta con nosotros para proteger tu vehículo</h2>

                <p className="confirmation__content__text">Enviaremos la confirmación de compra de tu Plan Vehícular Tracking a tu correo:</p>
                <p className="confirmation__content__email">{person?.email}</p>

                <button className="confirmation__content__button">CÓMO USAR MI SEGURO</button>
            </div>
            <div className="confirmation__content__copyright--block">
                <small className="confirmation__content__copyright">&copy; {AppSettings.COPYRIGHT}</small>
            </div>
        </div>
    )
}
