import React from 'react';
import { images } from '../../library/utils/getImages';

export const ConfirmationImage = () => {
    return (
        <div className="confirmation__image">
            <div className="confirmation__image__background">
                <img className="mobile" src={images('./confirmation_background_mobile.svg').default} alt="background"/>
                <img className="desktop" src={images('./confirmation_background_desktop.svg').default} alt="background"/>
            </div>
            <img className="person mobile" src={images('./confirmation_person_mobile.svg').default} alt="background"/>
            <img className="person desktop" src={images('./confirmation_person_desktop.svg').default} alt="background"/>
        </div>
    )
}
