import { Collapse, FormControlLabel, Switch } from '@mui/material';
import { styled } from '@mui/material/styles';
import React, { useState } from 'react';
import { images } from '../../library/utils/getImages';
import { CoverageItemModel } from '../../models/coverage.model';

const CustomSwitch = styled((props: any) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props}/>
  ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
      padding: 0,
      margin: 2,
      transitionDuration: '300ms',
      '&.Mui-checked': {
        transform: 'translateX(16px)',
        color: '#fff',
        '& + .MuiSwitch-track': {
          backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
          opacity: 1,
          border: 0,
        },
        '&.Mui-disabled + .MuiSwitch-track': {
          opacity: 0.5,
        },
      },
      '&.Mui-focusVisible .MuiSwitch-thumb': {
        color: '#33cf4d',
        border: '6px solid #fff',
      },
      '&.Mui-disabled .MuiSwitch-thumb': {
        color:
          theme.palette.mode === 'light'
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
      },
      '&.Mui-disabled + .MuiSwitch-track': {
        opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
      },
    },
    '& .MuiSwitch-thumb': {
      boxSizing: 'border-box',
      width: 22,
      height: 22,
    },
    '& .MuiSwitch-track': {
      borderRadius: 26 / 2,
      backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
      opacity: 1,
      transition: theme.transitions.create(['background-color'], {
        duration: 500,
      }),
    },
}))

type PlanCoverageItemProps = {
    coverageItem: CoverageItemModel;
    handleSelect: ()=> void;
    isAdded: boolean
}

export const PlanCoverageItem = ({coverageItem, handleSelect, isAdded}: PlanCoverageItemProps) => {
    const [open, setOpen] = useState(false);

    const handleCollapseClick = () => {
        setOpen(!open);
    };

    return (
        <div className="plan__content__coverage__item">
            <img className="plan__content__coverage__item__icon" src={images('./coverage_icon.svg').default} alt="logo"/>
            <div className="plan__content__coverage__item__content">
                <div className="plan__content__coverage__item__header">
                    <p>{coverageItem.name}</p>
                    <FormControlLabel
                        control={<CustomSwitch checked={isAdded}/>}
                        label=""
                        sx={{
                            marginRight: 0
                        }}
                        className="switch"
                        onClick={handleSelect}
                    />
                </div>
                {
                    isAdded && (
                        <div className="plan__content__coverage__item__action" onClick={handleSelect}>
                            <img src={images('./icon_remove.svg').default} alt="remove"/>
                            <p>QUITAR</p>
                        </div>
                    )
                }
                {
                    isAdded || (
                        <div className="plan__content__coverage__item__action" onClick={handleSelect}>
                            <img src={images('./icon_add.svg').default} alt="add"/>
                            <p>AGREGAR</p>
                        </div>
                    )
                }
                <Collapse in={open} timeout="auto" unmountOnExit className="plan__content__coverage__item__collapse">
                    <p>{coverageItem.description}</p>
                </Collapse>
                <button onClick={handleCollapseClick} className="plan__content__coverage__item__button mobile" style={{color: open?'#A3ABCC':'#6F7DFF'}}>
                    Ver {open?'menos':'más'}
                    <img className="icon" src={images(`./chevrot_${open?'less':'more'}.svg`).default} alt="arrow"/>
                </button>
            </div>
            <button onClick={handleCollapseClick} className="plan__content__coverage__item__button desktop">
                <img className="icon" src={images(`./chevrot_desktop.svg`).default} alt="arrow" style={{transform: `rotate(${open?0:180}deg)`}}/>
            </button>
        </div>
    )
}
