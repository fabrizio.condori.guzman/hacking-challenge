import React from 'react';
import { images } from '../../library/utils/getImages';

type PlanAmountBlockProps = {
    amount: number,
    handleClick: () => void
}

export const PlanAmountBlock = ({amount, handleClick}: PlanAmountBlockProps) => {

    return (
        <div className="plan__content__amount">
            <div className="plan__content__amount__text--block">
                <p className="plan__content__amount__text--m">MONTO</p>
                <p className="plan__content__amount__text--b">${amount.toFixed(2)}</p>
                <p className="plan__content__amount__text--s">mensuales</p>
            </div>
            <hr />
            <div className="plan__content__amount__description">
                <p className="plan__content__amount__description__text">El precio incluye:</p>
                <div className="plan__content__amount__description__item">
                    <img src={images('./icon_check.svg').default} alt="check-icon"/>
                    <span>Llanta de respuesto</span>
                </div>
                <div className="plan__content__amount__description__item">
                    <img src={images('./icon_check.svg').default} alt="check-icon"/>
                    <span>Analisis de motor</span>
                </div>
                <div className="plan__content__amount__description__item">
                    <img src={images('./icon_check.svg').default} alt="check-icon"/>
                    <span>Aros gratis</span>
                </div>
            </div>
            <button className="plan__content__amount__button" onClick={handleClick}>LO QUIERO</button>
        </div>
    )
}
