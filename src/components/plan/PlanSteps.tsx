import React from 'react';
import { useContext } from '../../context/quote-context';
import { images } from '../../library/utils/getImages';

type StepProps = {
    number: number,
    text: string;
    current: boolean;
    lastItem: boolean
}
const Step = ({number, text, current, lastItem}: StepProps) => {
    return (
        <div className={`step ${current?'current':''}`}>
          <div className="number--content">
            <div className="number">{number}</div>
            {
                lastItem || (<div className="dash-line"></div>)
            }
          </div>
          <p className="text">{text}</p>
        </div>
    )
}

export const PlanStepMobile = () => {
    const { person, setPerson, setStep} = useContext()
    const steps = ['Datos', 'Arma tu plan']
    const currentStep = 2;


    const handleBackButton = () => {
        setPerson(null)
        setStep('HOME')
    }

    return (
        <div className="plan__steps--mobile">
            <div className="content">
                <button className="icon" onClick={handleBackButton}><img src={images('./icon_back_grey.svg').default} alt="logo"/></button>
                <p className="step">PASO {currentStep} de {steps.length}</p>
                <div className="progress-bar">
                    <div className="current" style={{width: `${(currentStep/steps.length) *100}%`}}></div>
                </div>

            </div>
        </div>
    )
}

export const PlanStepDesktop = () => {
    const steps = ['Datos', 'Arma tu plan']
    const currentStep = 2;

    return (
        <div className="plan__steps--desktop">
            <div className="content">
                {
                    steps.map((step, index) => (
                        <Step key={index} number={index+1} text={step} current={index+1 === currentStep} lastItem={index+1 === steps.length}/>
                    ))
                }
            </div>
        </div>
    )
}
