import { CircularProgress } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useContext } from '../../context/quote-context';
import { AppSettings } from '../../library/settings';
import { formatNumberToCurrency } from '../../library/utils/formatNumber';
import { images } from '../../library/utils/getImages';
import { Car } from '../../models/car.model';
import { CoverageItemModel, CoverageTypeModel } from '../../models/coverage.model';
import { getCarDetailsByPlate, getCoverageData } from '../../services/quoteServices';
import { PlanAmountBlock } from './PlanAmountBlock';
import { PlanCoverageItem } from './PlanCoverageItem';
import { PlanCoverageType } from './PlanCoverageType';


export const PlanContent = () => {
    const { person, setPerson, setStep, setAmount} = useContext()
    
    const [coverageData, setCoverageData] = useState<CoverageTypeModel[]>([])
    const [car, setCar] = useState<Car>()
    const [loading, setLoading] = useState(false)
    const [coverageTypeIndex, setCoverageTypeIndex] = useState<number>(0)
    const [selectedCoverageItemIDList, setSelectedCoverageItemIDList] = useState<number[]>([])
    const [coverageItems, setCoverageItems] = useState<CoverageItemModel[]>([])
    const [baseAmount, setBaseAmount] = useState<number>(12500)
    const [monthlyAmount, setMonthlyAmount] = useState<number>(20)

    useEffect(() => {
        setLoading(true)
        setCoverageData(getCoverageData())
        getCarDetailsByPlate(person!.plate)
            .then(data => setCar(data))
            .finally(() => setLoading(false))
    }, [])

    useEffect(() => {
        setCoverageItems(coverageData[coverageTypeIndex]?.items || [])
    }, [coverageTypeIndex, coverageData])
    
    useEffect(() => {
        if(coverageTypeIndex !== 0) return

        const items = baseAmount > AppSettings.COVERAGE.limit_amount
                        ?coverageData[coverageTypeIndex]?.items.filter(i => i.id !== 2)
                        : coverageData[coverageTypeIndex]?.items
        setCoverageItems(items)

        if(baseAmount>AppSettings.COVERAGE.limit_amount){
            setSelectedCoverageItemIDList(selectedCoverageItemIDList.filter(ci => ci !== 2))
        }
    }, [baseAmount, coverageTypeIndex])
    
    useEffect(() => {
        const flatCoverageItems = coverageData.map(c => c?.items).flat()
        const selectedCoverages = flatCoverageItems.filter(ci => selectedCoverageItemIDList.includes(ci.id))
        const amount = AppSettings.COVERAGE.base_monthly_amount + selectedCoverages.reduce((a, b) => a+=b.amount,0)
        
        setMonthlyAmount(amount)
    }, [selectedCoverageItemIDList])

    
    const handleBackButton = () => {
        setPerson(null)
        setStep('HOME')
    }

    const handleSelectCoverageType = (index) => {
        if(index === coverageTypeIndex) return;

        setCoverageTypeIndex(index);
    }

    const handleBaseAmountAction = (amount) => {
        const newAmount = baseAmount+amount
        if(newAmount<AppSettings.COVERAGE.min_amount || newAmount>AppSettings.COVERAGE.max_amount)  return;
        setBaseAmount(baseAmount+amount)
    }

    const handleSelectCoverageItem = (itemID) => {
        const existOnList = selectedCoverageItemIDList.includes(itemID)
        if(existOnList){
            setSelectedCoverageItemIDList(selectedCoverageItemIDList.filter(ci => ci !== itemID))
        }else{
            setSelectedCoverageItemIDList([...selectedCoverageItemIDList, itemID])
        }
    }

    const handleNextStep = () => {
        setAmount(monthlyAmount)
        setStep('CONFIRMATION')
    }

    return (
        <>
            {
                loading && <div className="plan__content"><CircularProgress className="plan__content__loading"/></div>
            }
            {
                loading || (
                    <div className="plan__content">
                        <div className="plan__content--block">
                            <div className="back-action">
                                <button className="back-action__button" onClick={handleBackButton}>
                                    <img className="back-action__icon" src={images('./icon_back.svg').default} alt="back-icon"/>
                                </button>
                                <span className="back-action__text">VOLVER</span>
                            </div>

                            <div className="plan__content--background">
                                <div className="plan__content__greeting">
                                    <h2 className="plan__content__greeting__title mobile">Mira las coberturas</h2>
                                    <h2 className="plan__content__greeting__title desktop">¡Hola, {person!.name}!</h2>
                                    <p className="plan__content__greeting__text">Conoce las coberturas para tu plan</p>
                                </div>

                                <div className="plan__content__card">
                                    <div className="plan__content__card--block">
                                        <p className="plan__content__card__car-plate">Placa: {person!.plate}</p>
                                        <p className="plan__content__card__car-model">{`${car?.brand || ''} ${car?.year} ${car?.model}`}</p>
                                    </div>
                                    <img className="plan__content__card__image" src={images('./card_person.svg').default} alt="card-person"/>
                                </div>
                            </div>

                            <div className="plan__content__insurance">
                                
                                <div className="plan__content__insurance--block">
                                    <p className="plan__content__insurance__text">Indica la suma asegurada</p>
                                    <p className="plan__content__insurance__restrictions">MIN {`${formatNumberToCurrency(AppSettings.COVERAGE.min_amount)}`} <span>|</span> MAX {`${formatNumberToCurrency(AppSettings.COVERAGE.max_amount)}`}</p>
                                </div>
                                <div className="plan__content__insurance__amount">
                                    <button onClick={() => handleBaseAmountAction(-100)}>-</button>
                                    <p className="plan__content__insurance__amount__text">{formatNumberToCurrency(baseAmount)}</p>
                                    <button onClick={() => handleBaseAmountAction(100)}>+</button>
                                </div>
                            </div>

                            <hr />

                            <div className="plan__content__coverage">
                                <p className="plan__content__coverage__title">Agrega o quita coberturas</p>

                                <div className="plan__content__coverage__type--block">
                                    {
                                        coverageData.map( (c, index) => (
                                            <PlanCoverageType
                                                key={c.id} 
                                                handleSelect={() => handleSelectCoverageType(index)}
                                                coverageItem={c}
                                                isActive={coverageTypeIndex === index}
                                            />

                                        ))
                                    }
                                </div>
                                <div className="plan__content__coverage__item--block">
                                    {
                                        coverageItems?.map( (item) => (
                                            <PlanCoverageItem 
                                                key={item.id} 
                                                coverageItem={item} 
                                                handleSelect={() => handleSelectCoverageItem(item.id)} 
                                                isAdded={!!selectedCoverageItemIDList.includes(item.id)}
                                            />
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <PlanAmountBlock amount={monthlyAmount} handleClick={handleNextStep}/>
                    </div>
                )
            }
        </>
    )
}
