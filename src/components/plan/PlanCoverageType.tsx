import React from 'react';
import { CoverageTypeModel } from '../../models/coverage.model';

type PlanCoverageTypeProps = {
    coverageItem: CoverageTypeModel,
    isActive: boolean,
    handleSelect: ()=>void
}

export const PlanCoverageType = ({coverageItem, isActive, handleSelect}: PlanCoverageTypeProps) => {
    return (
        <div 
            key={coverageItem.id} 
            className={`plan__content__coverage__type ${isActive?'active':''}`} 
            onClick={() => handleSelect()}
        >
            {coverageItem.name}
        </div>
    )
}
