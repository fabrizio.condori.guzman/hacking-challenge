import React, { CSSProperties, useEffect, useState } from 'react';
import { Header } from '../../library/common/components/Header';
import { PlanContent } from './PlanContent';
import { PlanStepDesktop, PlanStepMobile } from './PlanSteps';

export const PlanScreen = () => {
    const [isMobile, setIsMobile] = useState<boolean>(false)
    const headerStyles: CSSProperties = {
        backgroundColor: 'white',
        borderBottom: '1px solid #E4E8F7'
    }

    useEffect(() => {
        handleResize()
    }, [])


    function handleResize(){
        if (window.matchMedia("(min-width: 768px)").matches) {
            setIsMobile(false)
        } else {
            setIsMobile(true)
        }
    }
    useEffect(() => {
        window.addEventListener('resize', handleResize)
        return () => {
            window.removeEventListener('resize', handleResize)
        }
    }, [])

    return (
        <>
            <Header {...headerStyles}/>
            <div className="plan">
                {
                    isMobile && <PlanStepMobile/>
                }
                {
                    isMobile || <PlanStepDesktop/>
                }
                <PlanContent/>
            </div>
        </>
    )
}
