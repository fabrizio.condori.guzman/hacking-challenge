import React, { CSSProperties, useEffect, useState } from 'react';
import { Header } from '../../library/common/components/Header';
import { HomeForm } from './HomeForm';
import { HomePresentation } from './HomePresentation';

export const HomeScreen = () => {
    const [isMobile, setIsMobile] = useState<boolean>(false)

    useEffect(() => {
        handleResize()
    }, [])


    function handleResize(){
        if (window.matchMedia("(min-width: 768px)").matches) {
            setIsMobile(false)
        } else {
            setIsMobile(true)
        }
    }
    useEffect(() => {
        window.addEventListener('resize', handleResize)
        return () => {
            window.removeEventListener('resize', handleResize)
        }
    }, [])

    const headerStyles: CSSProperties = {
        backgroundColor: isMobile?'#F7F8FC':'transparent',
    }

    return (
        <div className="home">
            <Header {...headerStyles}/>
            <HomePresentation/>
            <HomeForm/>
        </div>
    )
}
