import { Checkbox, FormControlLabel, MenuItem, Select, TextField } from '@mui/material';
import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useContext } from '../../context/quote-context';
import { Person } from '../../models/person.model';
import { getUser } from '../../services/quoteServices';

type FormData = {
    documentType: number;
    documentNumber: string;
    phone: string;
    plate: string;
    conditions: boolean
}


export const HomeForm = () => {
    const {setPerson, setStep} = useContext()
    const { register, handleSubmit, formState: { isValid }, control, setValue } = useForm<FormData>({mode: "onChange"});
    const [loading, setLoading] = useState(false)

    console.log('render')

    const onSubmit = handleSubmit(data => {
        setLoading(true)
        getUser().then((person: Person) => {
            person.plate = data.plate;
            setPerson(person)
            setStep('PLAN')
        }).finally(() => setLoading(false))
    });

    const validateInputValueAsNumber = (key: keyof FormData, e)=> {
        let phone = e.target.value;
        setValue(key, phone.replace(/[^0-9,.]+/g, ""));
    }

    return (
        <div className="home__information">
            <h2 className="home__information__title">Déjanos tus datos</h2>
            <form className="home__information__form" onSubmit={onSubmit}>
                <div className="home__information__input--group">
                    <Controller
                        control={control}
                        name="documentType"
                        defaultValue={1}
                        rules={{ required: true }}
                        render={({
                            field: { onChange, value },
                        }) => (
                            <Select
                                inputProps={{ 'aria-label': 'Without label' }}
                                sx={{
                                    border: '0px !important',
                                    '.MuiOutlinedInput-notchedOutline, .MuiOutlinedInput-notchedOutline:hover':{
                                        border: 0,
                                        borderRight: '1px solid #C5CBE0',
                                        borderRadius: 0
                                    },
                                    '.MuiOutlinedInput-notchedOutline.MuiOutlinedInput-notchedOutline': {
                                        border: 0,
                                        borderRight: '1px solid #C5CBE0',
                                        borderRadius: 0
                                    }
                                }}
                                value={value}
                                onChange={onChange}
                            >
                                <MenuItem value={1} selected>DNI</MenuItem>
                                <MenuItem value={2}>CE</MenuItem>
                            </Select>
                        )}
                    />
                    <Controller
                        control={control}
                        name="documentNumber"
                        defaultValue={''}
                        rules={{required: true, minLength: 8, maxLength: 8, pattern: /^[0-9]*$/ }}
                        render={({ field: { onChange, value } }) => (
                            <TextField
                                label="Nro. de doc"
                                type="text"
                                variant="filled"
                                value={value}
                                onChange={(v) => {onChange(v); validateInputValueAsNumber('documentNumber', v)}}
                                sx={{
                                    width: '100%',   
                                    '.MuiFilledInput-root, .MuiFilledInput-root:hover, .MuiFilledInput-root.Mui-focused':{
                                        backgroundColor: 'transparent',
                                        width: '100%'
                                    },
                                    '.MuiFilledInput-root::before, .MuiInputBase-root.MuiFilledInput-root::after': {
                                        borderBottom: 'none !important',
                                    },
                                    '.css-cio0x1-MuiInputBase-root-MuiFilledInput-root:after': {
                                        borderBottom: 'none !important',
                                    }
                                }}
                                InputProps={{ inputProps: { maxLength: 8 } }}
                            />      
                        )}
                    />
                </div>
                <Controller
                    control={control}
                    name="phone"
                    defaultValue={''}
                    rules={{required: true, minLength: 9, maxLength: 9, pattern: /^[0-9]*$/}}
                    render={({ field: { onChange, value } }) => (
                        <TextField
                            label="Celular"
                            type="text"
                            value={value}
                            onChange={(v) => {onChange(v); validateInputValueAsNumber('phone', v)}}
                            variant="filled"
                            inputProps={{ maxLength: 9 }}
                            className="custom-input home__information__input"
                            sx={{
                                '.MuiFilledInput-root, .MuiFilledInput-root:hover':{
                                    backgroundColor: 'transparent',
                                    width: '100%'
                                },
                                '.MuiFilledInput-root::before, .MuiInputBase-root.MuiFilledInput-root::after': {
                                    borderBottom: 'none !important',
                                },
                                '.css-cio0x1-MuiInputBase-root-MuiFilledInput-root:after': {
                                    borderBottom: 'none !important',
                                }
                            }}
                        />
                    )}
                />
                <Controller
                    control={control}
                    name="plate"
                    defaultValue={''}
                    rules={{required: true, minLength: 7, maxLength: 7}}
                    render={({ field: { onChange, value } }) => (
                        <TextField
                            label="Placa"
                            type="text"
                            variant="filled"
                            className="custom-input home__information__input"
                            value={value}
                            onChange={onChange}
                            onInput = {(e: any) =>{
                                e.target.value = e.target.value.slice(0,7)
                            }}
                            sx={{
                                '.MuiFilledInput-root, .MuiFilledInput-root:hover':{
                                    backgroundColor: 'transparent',
                                    width: '100%'
                                },
                                '.MuiFilledInput-root::before, .MuiInputBase-root.MuiFilledInput-root::after': {
                                    borderBottom: 'none !important',
                                },
                                '.css-cio0x1-MuiInputBase-root-MuiFilledInput-root:after': {
                                    borderBottom: 'none !important',
                                }
                            }}
                        />
                    )}
                />

                <FormControlLabel 
                    className="home__information__input--check"
                    control={<Checkbox />} 
                    label={<p>Acepto la <a href="#">Política de Protecciòn de Datos Personales</a> y los <a href="#">Términos y Condiciones</a>.</p>} 
                    sx={{
                        display: 'flex',
                        alignItems: 'flex-start',
                        margin: 0,
                        '.MuiCheckbox-root': {
                            padding: 0,
                            marginRight: '12px',
                            color: '#43B748 !important'
                        },
                    }}
                    {...register('conditions', {required: true})}
                />

                <button className="home__information__button" disabled={!isValid || loading}>Cotízalo</button>
            </form>
        </div>
    )
}
