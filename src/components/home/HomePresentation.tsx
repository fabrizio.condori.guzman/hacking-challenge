import React from 'react';
import { AppSettings } from '../../library/settings';
import { images } from '../../library/utils/getImages';

export const HomePresentation = () => {
    return (
        <div className="home__presentation">
            <div className="home__presentation__content">
                <img src={images('./home_desktop.svg').default} alt="logo" className="home__presentation__image desktop"/>  
                <div className="home__presentation__text--block">
                    <p className="home__presentation__text--small">¡NUEVO!</p>
                    <h2 className="home__presentation__text--title">Seguro Vehicular <span>Tracking</span></h2>
                    <p className="home__presentation__text--description">Cuentanos donde le haras seguimiento a tu seguro</p>
                </div>
                <img src={images('./home_mobile.svg').default} alt="logo" className="home__presentation__image mobile"/>  
            </div>


            <small className="home__presentation__copyright">&copy; {AppSettings.COPYRIGHT}</small>
        </div>
    )
}
