export class AppSettings {
    public static COVERAGE = {
        base_monthly_amount: 20,
        limit_amount: 16000,
        min_amount: 12500,
        max_amount: 16500
    } as const;

    public static COPYRIGHT = '2021 RIMAC Seguros y Reaseguros.'
}