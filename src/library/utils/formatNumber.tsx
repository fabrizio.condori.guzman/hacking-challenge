export const formatNumberToCurrency = (number) => {
    return `$${new Intl.NumberFormat().format(number)}`
}