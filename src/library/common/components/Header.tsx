import React, { CSSProperties } from 'react'
import { images } from '../../utils/getImages'

export const Header = (styles: CSSProperties | undefined) => {
    return (
        <header className="header" style={styles}>
            <img src={images('./logo_rimac.svg').default} alt="logo"/>
            <div className="header__contact">
                <p className="header__contact__question">¿Tienes alguna duda?</p>
                <div className="header__contact__phone">
                    <img src={images('./ic_phone.svg').default} alt="icon-phone"/>
                    <p className="header__contact__phone__text desktop">(01) 411 6001</p>
                    <p className="header__contact__phone__text mobile">Llámanos</p>
                </div>
            </div>
        </header>
    )
}
