import React from 'react';
import ReactDOM from 'react-dom';
import { QuoteApp } from './QuoteApp';
import './styles/styles.scss';

ReactDOM.render(
  <React.StrictMode>
    <QuoteApp />
  </React.StrictMode>,
  document.getElementById('root')
);