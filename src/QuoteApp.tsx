import { createTheme, ThemeProvider } from '@mui/material';
import React from 'react';
import { QuoteContextProvider } from './context/quote-context';
import { QuotePage } from './pages/QuotePage';

export const QuoteApp = () => {
    const theme = createTheme({
        palette: {
            primary: {
                main: '#EF3340',
            },
        }
    });
    return (
        <ThemeProvider theme={theme}>
            <QuoteContextProvider>
                <QuotePage/>
            </QuoteContextProvider>
        </ThemeProvider>
    )
}
