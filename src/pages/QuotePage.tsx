import React from 'react';
import { ConfirmationScreen } from '../components/confirmation/ConfirmationScreen';
import { HomeScreen } from '../components/home/HomeScreen';
import { PlanScreen } from '../components/plan/PlanScreen';
import { useContext } from '../context/quote-context';

export const QuotePage = () => {
    const {step} = useContext()
    return (
        <>
            {step === 'HOME' && <HomeScreen/>}
            {step === 'PLAN' && <PlanScreen/>}
            {step === 'CONFIRMATION' && <ConfirmationScreen/>}
        </>
    )
}
