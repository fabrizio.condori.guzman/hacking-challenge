export interface CoverageTypeModel {
    id: number;
    name: string;
    items: any []
}

export interface CoverageItemModel {
    id: number;
    name: string;
    description: string;
    amount: number;
}