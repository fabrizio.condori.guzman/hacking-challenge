export interface Car{
    brand: string;
    year: number;
    model: string;
}